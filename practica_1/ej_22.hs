data Rosetree a = Rose a [Rosetree a]

foldRose :: (a -> [b] -> b) -> Rosetree a -> b
foldRose f (Rose a xs) = f a $ map (foldRose f) xs

hojas :: Eq a => (Rosetree a) -> [a]
hojas = foldRose (\x recs -> if recs == [[]] then [x] else concat recs)

distancias :: (Rosetree a) -> [Int]
distancias = foldRose (\x recs -> if recs == [[]] then [0] else map (+1)  (concat recs))

altura :: (Rosetree a) -> Int
altura = foldRose (\x recs -> 1 + (maximum recs))
