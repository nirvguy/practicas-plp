data Nat = Z | Suc Nat deriving (Show, Eq)

--a)
foldNat :: (b -> b) -> b -> Nat -> b
foldNat f b Z = b
foldNat f b (Suc z) = f (foldNat f b z)

--b)
suma :: Nat -> Nat -> Nat
suma = foldNat (\f_rec -> \m -> Suc (f_rec m)) id

producto :: Nat -> Nat -> Nat
producto = foldNat (\f_rec -> \m -> (f_rec m) `suma` m) (const Z)

potencia :: Nat -> Nat -> Nat
potencia n m = foldNat (\rec -> rec `producto` n) (Suc Z) m
