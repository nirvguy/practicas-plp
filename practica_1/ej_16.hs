import Data.List

type DivideConquer a b =     (a -> Bool)               -- Es caso base?
						  -> (a -> b)                  -- Resuelve caso trivial
						  -> (a -> [a])                -- Parte el problema en subproblemas
						  -> ([b] -> b)                -- Combina resultados
						  -> a                         -- Input
						  -> b                         -- Resultado

--a)
dc :: DivideConquer a b
dc esTrivial solve split combine x | esTrivial x = solve x
								   | otherwise = combine partidos
	where partidos = map (dc esTrivial solve split combine) (split x)

--b)

mergeSort :: Ord a => [a] -> [a]
mergeSort = dc (\xs -> (length xs) < 2) 
               id 
               (\xs -> [take (mitad xs) xs, drop (mitad xs) xs]) 
	           merge
	where mitad xs = (length xs) `div` 2

merge :: Ord a => [[a]] -> [a]
merge xss = foldr (\x rec -> \ys -> rec (insert x ys)) id (head xss) $ last xss

--c)

mapDc :: (a -> b) -> [a] -> [b]
mapDc f = dc (\xs -> (length xs) < 2) (\xs -> if null xs then [] else [f (head xs)])
             (\xs -> [[head xs], tail xs]) (concat)

filterDc :: (a -> Bool) -> [a] -> [a]
filterDc f = dc (\xs -> length xs < 2) (\xs -> if null xs then [] else (if f (head xs) then xs else []))
             (\xs -> [[head xs], tail xs]) (concat)
