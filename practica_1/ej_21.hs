data AB a = Nil | Bin (AB a) a (AB a)

--a)
foldAB :: (b -> a -> b -> b) -> b -> AB a -> b
foldAB f b Nil = b
foldAB f b (Bin i r d) = f (foldAB f b i) r (foldAB f b d)

--b)
esNil :: AB a -> Bool
esNil = foldAB (\reci x recr -> False) True

altura :: AB a -> Int
altura = foldAB (\reci x recr -> 1 + (max reci recr)) 0

ramas :: AB a -> [[a]]
ramas = foldAB (\reci x recr -> (map (x:) reci) ++ (map (x:) recr)) [[]]

espejo :: AB a -> AB a
espejo = foldAB (\reci x recr -> Bin recr x reci) Nil

--c)
left :: AB a -> AB a
left (Bin ri x rd) = ri

right :: AB a -> AB a
right (Bin ri x rd) = rd

mismaEstructura :: AB a -> AB b -> Bool
mismaEstructura = foldAB (\reci x recr -> (\r -> not (esNil r) && reci (left r) && recr (right r))) esNil
