data Polinomio a = X | Cte a | Suma (Polinomio a) (Polinomio a) | Prod (Polinomio a) (Polinomio a)

foldPoly :: (b -> b -> b) -> (b -> b -> b) -> (a -> b) -> b -> Polinomio a -> b
foldPoly f_suma f_prod f_cte b poly = case poly of
											X -> b
											Cte a -> f_cte a
											Suma p1 p2 -> f_suma (foldPoly f_suma f_prod f_cte b p1) (foldPoly f_suma f_prod f_cte b p2)
											Prod p1 p2 -> f_suma (foldPoly f_suma f_prod f_cte b p1) (foldPoly f_suma f_prod f_cte b p2)

evaluar :: Num a => a -> Polinomio a -> a
evaluar x = foldPoly (+) (*) (id) x
