--a)
partes :: [Int] -> [[Int]]
partes l = foldr f [[]] l
    where f x ys = [x:j | j <- ys]++ys

--b)
prefijos :: [Int] -> [[Int]]
prefijos = foldr f [[]]
    where f x y = [x]:[x:ys | ys <- y, ys /= []]
