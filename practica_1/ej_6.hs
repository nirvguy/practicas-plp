partir :: [a] -> [([a],[a])]
partir xs = [(take x xs,drop x xs) | x <- [0..length xs]]
