-- En cualquier iteración siempre a=1,b=1,2,3,...
-- 
import Data.List
ternapitagorica = nub [ (a,b,c) | s <- [1..], a <- [1..s], b <- [1..s], c <- [1..s], a^2+b^2==c^2 ]
