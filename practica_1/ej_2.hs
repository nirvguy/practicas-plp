-- a)
curry :: ((a,b) -> c) -> (a -> b -> c)
curry g = (\x y -> g (x,y))

-- b)
uncurry :: (a -> b -> c) -> ((a,b) -> c)
uncurry g = \t -> g (fst t) (snd t)

--c) 
-- No se puede expresar una tupla con una cantidad arbitraria de elementos
