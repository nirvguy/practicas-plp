esPrimo :: Int -> Bool
esPrimo x = (x > 1) && length (filter (\y -> x `mod` y == 0) [2..x-1]) == 0

primos = take 1000 [ x | x <- [1..], esPrimo x]
