--a)
sacarUna :: Eq a => a -> [a] -> [a]
sacarUna a xs = (fst partida)++(tail (snd partida))
	where partida = break (==a) xs

--b)
permutaciones :: [a] -> [[a]]
permutaciones = foldr (\x rec -> [(take n ps)++[x]++(drop n ps) | n <-[0..length (head rec)], ps <- rec]) [[]]

-- alternativa:
-- permutaciones []     = [[]]
-- permutaciones (x:xs) =  [ (take n ps)++[x]++(drop n ps) | n <- [0..length xs], ps <- permutaciones xs]
