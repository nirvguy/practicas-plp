-- a)
sumatoria = foldr + 0
pertenece e = foldr (\x y -> x == e || y) False
--
concatenarAux :: [a] -> [a] -> [a]
concatenarAux l1 = foldr (:) l1
concatenar :: [a] -> [a] -> [a]
concatenar = flip concatenarAux
--
filtrar :: (a -> Bool) -> [a] -> [a]
filtrar f = foldr (\x ys -> if f x then ys else x:ys) []
--
mapear :: (a -> b) -> [a] -> [b]
mapear f = foldr (\x ys -> (f x):ys) []

--b)
sumaAlt :: [Int] -> Int
sumaAlt l = (foldr (+) 0 [l!!i | i <- [0..(length l)-1], even i] ) -
            (foldr (+) 0 [l!!i | i <- [0..(length l)-1], not (even i)])
          -- = sum [l!!i | i <- [0..(length l)-1], even i] -
          --   sum [l!!i | i <- [0..(length l)-1], not(even i)]
