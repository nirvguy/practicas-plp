generate :: ([a] -> Bool) -> ([a] -> a) -> [a]
generate stop next = generateFrom stop next []

generateFrom :: ([a] -> Bool) -> ([a]->a) -> [a] -> [a]
generateFrom stop next xs | stop xs = init xs
                          | otherwise = generateFrom stop next (xs ++ [next xs])

--a)
generateBase :: ([a] -> Bool) -> a -> (a -> a) -> [a]
generateBase stop b next = generate stop (\xs -> if null xs then b else next (last xs))

--b)
factoriales :: Int -> [Int]
factoriales n = generate (\xs -> length xs > n) (\xs -> if null xs then 1 else (last xs)*((length xs) + 1))

--c)
iterateN :: Int -> (a -> a) -> a -> [a]
iterateN n f b = generateBase (\xs -> length xs > n) b f


--d)
-- generateFrom2 :: ([a] -> Bool) -> ([a]->a) -> [a] -> [a]
-- generateFrom2 stop next xs = last $ takeWhile (not.stop) $ iterate (\ys -> ys++[next ys]) xs
