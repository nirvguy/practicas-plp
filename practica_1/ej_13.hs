--a)
mapPares :: (a -> b -> c) -> [(a,b)] -> [c]
mapPares f = foldr (\(x,y) rec -> (f x y):rec) [] 

--b)
armarPares :: [a] -> [b] -> [(a,b)]
armarPares xs = foldr (\x rec -> \ys -> if null ys then 
                                           [] 
                                        else 
                                           (x, head ys) : rec (tail ys)
                      ) (const []) xs

--c)
mapDoble :: (a -> b -> c) -> [a] -> [b] -> [c]
mapDoble f xs ys = map (uncurry f) $ armarPares xs ys
