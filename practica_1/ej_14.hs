--a)
sumMat :: [[Int]] -> [[Int]] -> [[Int]]
sumMat = zipWith (zipWith (+))

--b)
trasponer :: [[a]] -> [[a]]
trasponer = foldr (zipWith (:)) [[] | x <- [1..]]

--c)
zipWithList :: (a -> b -> b) -> b -> [[a]] -> [b]
zipWithList f b xs = map (foldr f b) (trasponer xs)
