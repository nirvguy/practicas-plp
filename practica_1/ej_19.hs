type Conj a = (a -> Bool)

--a)
vacio :: Conj a
vacio = const False

--b)
agregar :: Eq a => a -> Conj a -> Conj a
agregar a c = \x -> x == a || c x

interseccion :: Conj a -> Conj a -> Conj a
interseccion c c' = \x -> c x && c' x

union :: Conj a -> Conj a -> Conj a
union c c' = \x -> c x || c' x

--c)
conjInfinitos = [(\x -> x `mod` i == 0) | i<-[1..]]

--d)
primeraAparicion :: a -> [Conj a] -> Int
primeraAparicion x cs = head [ i | i <- [1..], (cs !! i) x]
