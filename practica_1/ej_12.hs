-- --a)
genLista :: Eq a => (a -> a) -> Int -> a -> [a]
genLista f n x0 = foldl (\acum x -> if acum == [] then [x0] else acum++[f (last acum)]) [] [1..n]
--
-- alternativa:
-- componerN :: Int ->  (a -> a) -> (a -> a)
-- componerN n f = foldr (\_ rec -> rec . f) id [1..n]
--
-- genListaInfinita :: Eq a => (a -> a) -> a -> [a]
-- genListaInfinita f x0 = map (\x -> (componerN (x-1) f) x0) [1..]
--
-- genLista :: Eq a => (a -> a) -> Int -> a -> [a]
-- genLista f n x0 = take n (genListaInfinita f x0)

--b)
desdeHasta :: Int -> Int -> [Int]
desdeHasta n m = genLista (+1) (m-n+1) n
