--a)
-- Solucion 1:
limpiar :: String -> String -> String
limpiar [] c = c
limpiar (s:ss) c = limpiar ss (filter (/=s) c)


-- Solucion 2:
-- limpiar :: String -> String -> String
-- limpiar [] c = c
-- limpiar (s:ss) c = limpiar ss (limpiarC s c)
--
-- limpiarC :: Char -> String -> String
-- limpiarC c [] = []
-- limpiarC c (x:xs) = (if c == x then [] else [x]) ++ (limpiarC c xs)

-- b)
diffPromedio :: [Float] -> [Float]
diffPromedio fs = map (\x -> x - promedio) fs
    where promedio = (sum fs) / (fromIntegral (length fs))


-- c)
todosIguales :: [Int] -> Bool
todosIguales [] = True
todosIguales (x:[]) = True
todosIguales (x:xs) = (x == head(xs)) && todosIguales(xs)
