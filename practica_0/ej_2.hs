-- a)
valorAbsoluto :: Int -> Int
valorAbsoluto x
     | x >= 0    =  x
     | otherwise = -x

-- b)
bisiesto :: Int -> Bool
bisiesto a = (a `mod` 4 == 0) && (a `mod` 100 /= 0) || (a `mod` 400 == 0)

-- c)
factorial :: Int -> Int
factorial 0 = 1
factorial n = n * factorial (n-1)

-- d)
-- Solucion 1 para esPrimo
esPrimo :: Int -> Bool
esPrimo x = (x > 1) && foldr f True [2..x-1]
    where f y z = (x `mod` y /= 0) && z

-- Solucion 2 para esPrimo
-- esPrimo :: Int -> Bool
-- esPrimo x = (x > 1) && length (filter (\y -> x `mod` y == 0) [2..x-1]) == 0

cantDivisoresPrimos :: Int -> Int
cantDivisoresPrimos x = length (filter f [1..x])
    where f y = (esPrimo y) && x `mod` y == 0
