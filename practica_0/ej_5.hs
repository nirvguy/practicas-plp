data AB a = Nil | Bin (AB a) a (AB a)

-- a)
vacioAB :: AB a -> Bool
vacioAB Nil = True
vacioAB _   = False

-- b)
negacionAB :: AB Bool -> AB Bool
negacionAB Nil = Nil
negacionAB (Bin x v y) = Bin (negacionAB x) (not v) (negacionAB y)

-- c)
productoAB :: AB Int -> Int
productoAB Nil = 1
productoAB (Bin x v y) = (productoAB x) * v * (productoAB y)
