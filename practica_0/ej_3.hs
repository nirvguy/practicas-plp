-- a)
inverso :: Float -> Maybe Float
inverso 0 = Nothing
inverso x = Just (1.0 / x)
