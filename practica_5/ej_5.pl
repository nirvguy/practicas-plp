last2([U],U).
last2([_|Ys],U) :- last(Ys,U).

reverse2([],[]).
reverse2([X|Xs],[R|Rs]) :- last(Rs,X), last(Xs,R), reverse(Xs,Rs).

maxLista([X],X).
maxLista([X|Xs],U) :- maxLista(Xs,Z), U is max(X,Z).

minLista([X],X).
minLista([X|Xs],U) :- minLista(Xs,Z), U is min(X,Z).

prefijo(X,Y) :- append(X,_,Y).

sufijo(X,Y) :- append(_,X,Y).

sublista(X,Y) :- prefijo(Z,Y), sufijo(X,Z).

pertenece(X,[X|Xs]).
pertenece(Y,[X|Xs]) :- pertenece(Y,Xs).
