inorder(nil,[]).
inorder(bin(I,V,D),R) :- inorder(I,RI), inorder(D,RD), append(RI,[V|RD],R).

arbolConInorder([],nil).
arbolConInorder(S,bin(I,V,D)) :- append(L,[V|R],S), 
								 arbolConInorder(L,I), 
								 arbolConInorder(R,D).

aBB(nil).
aBB(bin(nil,_,nil)).
aBB(bin(bin(II,V1,ID),V2,nil)) :- V1 =< V2, aBB(bin(II,V1,ID)).
aBB(bin(nil,V2,bin(DI,V3,DD))) :- V2 =< V3, aBB(bin(DI,V3,DD)).
aBB(bin(bin(II,V1,ID),V2,bin(DI,V3,DD))) :- V1 =< V2, V2 =< V3, 
											aBB(bin(II,V1,ID)),
											aBB(bin(DI,V3,DD)).
%Otra forma mas simple: 
%aBB(T) :- inorder(T,L), sort(L,L).

aBBInsertar(X, nil, bin(nil,X,nil)).
aBBInsertar(X, bin(I,V,D), bin(RI,V,D)) :- X =< V, aBBInsertar(X,I,RI).
aBBInsertar(X, bin(I,V,D), bin(I,V,RD)) :- X > V, aBBInsertar(X,D,RD).
