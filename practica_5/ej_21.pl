:- [ej_9].
esNodo(g,v1).
esNodo(g,v2).
esNodo(g,v3).
esNodo(g,v4).
esEje(g,v1,v2).
esEje(g,v2,v4).
esEje(g,v4,v3).
esEje(g,v1,v3).
esEje(g,v3,v4).

esNodo(g2,v1).
esNodo(g2,v2).

esEje(g2,v1,v2).
esEje(g2,v2,v1).

esCamino(G,D,D,[D]).
esCamino(G,D,H,[D,L|Ls]) :- esEje(G,D,L), esCamino(G,L,H,[L|Ls]).

caminoHamiltoniano(G,L) :- esNodo(G,V1), esNodo(G,V2), esCamino(G,V1,V2,L), not((esNodo(G,V3),not(member(V3,L)))).

esConexo(G) :- not((esNodo(G,V1),esNodo(G,V2),not(esCamino(G,V1,V2,L)))).
