%interseccion(+L1,+L2,-R)
interseccion([],_,[]).
interseccion([L|Ls],M,[L|Rs]) :- member(L,M), interseccion(Ls,M,Rs), !.
interseccion([_|Ls],M,Rs) :- interseccion(Ls,M,Rs).

%split2(?N,?L,?L1,-L2)
split2(N,L,L1,L2) :- append(L1,L2,L), length(L1,N), length(L,Z), S is Z-N, length(L2,S).

%borrar(?L,?E,?S)
borrar([],_,[]).
borrar([L|Ls],X,Rs) :- X == L, borrar(Ls,X,Rs).
borrar([L|Ls],X,[L|Rs]) :- X \== L, borrar(Ls,X,Rs).

%sacarDuplicados(+L1,-L2)
sacarDuplicados([],[]).
sacarDuplicados([X|Xs],Ls) :- borrar(Xs,X,L1), sacarDuplicados(L1,L2), append([X],L2,Ls).

%reparto(+L,+N,-LListas)
reparto([],0,[]).
reparto(L,1,[L]).
reparto(L,N,[[]|R]) :- N > 1, N2 is N-1, reparto(L,N2,R).
reparto([L|Ls],N,[[L|S]|Rs]) :- N > 1, reparto(Ls,N,[S|Rs]).


%repartoSinVacia(+L,-LListas)
repartoSinVacias(Ls,R) :- length(Ls,N), between(1,N,Z), reparto(Ls,Z,R), borrar(R,[],R).
