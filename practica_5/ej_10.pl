intercalar([],L,L).
intercalar(L,[],L) :- length(L,Z), Z >= 1.
intercalar([L|Ls],[M|Ms],[L,M|R]) :- intercalar(Ls,Ms,R).
