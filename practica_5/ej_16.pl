:- [ej_13].

subABB(nil,nil).
subABB(bin(I,_,_), RI) :- subABB(I,RI).
subABB(bin(I,_,D), RD) :- subABB(D,RD).
subABB(bin(I,V,D), bin(RI,V,RD)) :- subABB(I,RI), subABB(D,RD), aBB(bin(RI,V,RD)).
