aplanar([],[]).
aplanar([L|Ls],Rs) :- aplanar(Ls,S), aplanar(L,R), append(S,R,Rs), !.
aplanar([L|Ls],[L|Rs]) :- aplanar(Ls,Rs).
