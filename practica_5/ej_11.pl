:- [ej_9].
% elementosTomadosEnOrden(+L,+N,-Elementos)

primeros([],[]).
primeros([[X|_]|Xss],[X|Rs]) :- primeros(Xss,Rs).

elementosTomadosEnOrden(L,N,Rs) :-length(L,M), between(0,M,Z), split2(Z,L,_,L2), repartoSinVacias(L2,X), length(X,N), primeros(X,Rs).
