:- [ej_9].

diferenciaSimetrica(_,[],_).
diferenciaSimetrica(L1,[L|L2],[L3|LS3]) :- member(L3,L1), not((member(L3,L2))), diferenciaSimetrica(L1,L2,LS3).
diferenciaSimetrica(L1,[L2|LS2],[L2|LS3]) :- not(member(L2,L1)), diferenciaSimetrica(L1,LS2,LS3).
