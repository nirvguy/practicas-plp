:- [ej_8].
:- [ej_9].
:- [ej_13].

arboles(0,nil).
arboles(N,bin(AI,Z,AD)) :- N2 is N-1, between(0,N2,X), arboles(X,AI), Y is N2-X, arboles(Y,AD).

arbol(A) :- desde(0,X), arboles(X,A).

incluido([],L2).
incluido([L|Ls],R) :- member(L,R), incluido(Ls,R).

nodosEn(A,L) :- inorder(A,L1), incluido(L1,L).

sinRepEn(A,L) :- length(L, Z), between(0, Z, X), arboles(X,A), inorder(A,L1), incluido(L1,L), sacarDuplicados(L1,L1).
