:- [ej_8].

esCompatible(A,B,C) :- D is B + C, A < D, E is A-B, F is abs(E), D > F.

esTriangulo(tri(A,B,C)) :- esCompatible(A,B,C), esCompatible(B,A,C), esCompatible(C,A,B).

perimetro(tri(A,B,C),P) :- desde(0,P), between(0,P,A), R is P-A, between(0,R,B), C is P-(A+B),
						   esTriangulo(tri(A,B,C)).
