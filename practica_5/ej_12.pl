vacio(nil).

raiz(bin(_,V,_),V).

altura(nil,0).
altura(bin(I,_,D),A) :- altura(I,AI), altura(D,AD), AI =< AD, A is AD+1.
altura(bin(I,_,D),A) :- altura(I,AI), altura(D,AD), AD < AI, A is AI+1.

cantidadDeNodos(nil,0).
cantidadDeNodos(bin(I,_,D),A) :- cantidadDeNodos(I,AI), cantidadDeNodos(D,AD), A is AI+AD+1.
