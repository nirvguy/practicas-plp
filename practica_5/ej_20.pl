:- [ej_9].

sumaLista([],0).
sumaLista([R|Rs],Z) :- sumaLista(Rs,T), Z is R+T.

diffCorte(L1,L2,Z) :- sumaLista(L1,Z1), sumaLista(L2,Z2), Z is abs(Z1-Z2).


corteMasParejo(L,L1,L2) :- length(L, S), between(0,S, N), split2(N,L,L1,L2), diffCorte(L1,L2,Z1),
                           not((between(0,S,N2), split2(N2,L,L3,L4), diffCorte(L3,L4,Z2), Z2 < Z1)).
