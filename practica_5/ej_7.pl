palindromo(L,R) :- reverse(L,Z), append(L,Z,R).

doble(L,R) :- append(L,L,R).

iesimo_aux(1,[L|_],L).
iesimo_aux(I,[_|Ls],R) :- J is I-1, iesimo_aux(J,Ls,R).

iesimo(I,Ls,R) :- length(Ls,S), between(1,S,I), iesimo_aux(I,Ls,R).
