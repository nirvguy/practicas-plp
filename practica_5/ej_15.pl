:- [ej_8].

listasQueSuman(0,0,[]).
listasQueSuman(N,M,[R|Rs]) :- M >= 1, between(0,N,R), M2 is M-1, NR is N-R, listasQueSuman(NR,M2,Rs).

cuadradoSemiLatinoQueSuma(N,M,1,[R]) :- listasQueSuman(N,M,R).
cuadradoSemiLatinoQueSuma(N,M,Z,[R|Rs]) :- Z >= 1, listasQueSuman(N,M,R), Z2 is Z-1, cuadradoSemiLatinoQueSuma(N,M,Z2,Rs).

cuadradoSemiLatino(N,L) :- desde(0,X), cuadradoSemiLatinoQueSuma(X,N,N,L).
